package com.abnamro.apps.referenceandroid.ui_tests.classification_tree_method_tests

import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.abnamro.apps.referenceandroid.TestHelper.assertElementExists
import com.abnamro.apps.referenceandroid.TestHelper.assertElementExistsAtPositionWithColor
import com.abnamro.apps.referenceandroid.TestHelper.assertElementNOTExistsAtPosition
import com.abnamro.apps.referenceandroid.TestHelper.assertListIsSortedBy
import com.abnamro.apps.referenceandroid.TestHelper.assertListSizeEqualsTo
import com.abnamro.apps.referenceandroid.TestHelper.scrollToPosition
import com.abnamro.apps.referenceandroid.TestHelper.seekBarValue
import com.abnamro.apps.referenceandroid.TestHelper.switch
import com.abnamro.apps.referenceandroid.TestHelper.tap
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.casesPerMillion
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.countryList
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.deathsPerMillion
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.red
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.deathRatioSeekBar
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.displayDetailsSwitch
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.searchButton
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.selectItemShowCountries
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.selectItemSorBySpinner
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.showCountriesSpinner
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.sortBySpinner
import com.abnamro.apps.referenceandroid.view.FilterAndSortActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class TestCase4 {

    /**
     * TEST CASE 1:
     * ------------------------------------------------------
     * Given I am on the 'Sort & Filter Countries screen
     * When I select 'cases' from 'Sort by' spinner
     * And I select 'all' from 'Show countries' spinner
     * And the 'Display details' switch is OFF
     * And the Seekbar 'highlight death-ratio' is 74
     * And I tap the search FAB
     * Then the 'Coronavirus per Country' screen is displayed
     * And I see a sorted list by cases of all countries
     * And I see detailed information
     * And highlights of death-cases above 74 are displayed
     * -----------------------------------------------------
     */

    @get:Rule
    //Open Sort & Filter screen
    var activityRule: ActivityTestRule<FilterAndSortActivity>
            = ActivityTestRule(FilterAndSortActivity::class.java)

    @Test
    fun filterAndSortTest() {
        // Select 'cases' from 'Sort by' spinner
        tap(sortBySpinner)
        selectItemSorBySpinner("cases")

        // Select 'all' from 'Show countries' spinner
        tap(showCountriesSpinner)
        selectItemShowCountries("all")

        // Turn display details switch off
        switch(displayDetailsSwitch, false)

        // seekbar value set to 74
        seekBarValue(deathRatioSeekBar, 74)

        // tap search button and check if country list screen is opened
        tap(searchButton)
        assertElementExists("Coronavirus per Country")

        // assert all sort and filter options
        assertListIsSortedBy(countryList, "Cases", 13)
        assertListSizeEqualsTo(countryList, 13) // 13 items in total in mock data
        assertElementNOTExistsAtPosition(countryList, 12, casesPerMillion)
        assertElementNOTExistsAtPosition(countryList, 12, deathsPerMillion)
        scrollToPosition(countryList, 1)
        assertElementExistsAtPositionWithColor(countryList, 1, red)

    }
}
