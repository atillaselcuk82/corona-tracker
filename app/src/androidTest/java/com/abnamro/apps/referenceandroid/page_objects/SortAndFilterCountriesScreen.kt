package com.abnamro.apps.referenceandroid.page_objects

import com.abnamro.apps.referenceandroid.R
import com.abnamro.apps.referenceandroid.TestHelper.tapItem
import com.abnamro.apps.referenceandroid.TextId

object SortAndFilterCountriesScreen {

    const val titleSortAndFilterCountries = "Sort & Filter Countries"

    //Sort by elements
    val sortByText = TextId(R.id.sort_by_tv, "Sort by:")
    const val sortBySpinner = R.id.sort_by_spinner

    //Show countries elements
    val showCoutriesText = TextId(R.id.show_countries_tv, "Show countries:")
    const val showCountriesSpinner = R.id.show_countries_spinner

    //Display details elements
    val displayDetailsText = TextId(R.id.display_details_tv, "Display details")
    const val displayDetailsSwitch = R.id.display_details_switch

    //Highlight death-ratio elements
    val highlightDeathRatioText = TextId(R.id.highlight_death_ratio_tv, "Highlight death-ratio higher then:")
    const val deathRatioSeekBar = R.id.death_ratio_seek_bar

    const val searchButton = R.id.fab_search

    fun selectItemSorBySpinner(item: String) {
        when(item){
            "cases" -> tapItem(0)
            "deaths" -> tapItem(1)
            "recovered" -> tapItem(2)
        }
    }

    fun selectItemShowCountries(item: String) {
        when(item){
            "all" -> tapItem(0)
            "top 10" -> tapItem(1)
            "top 3" -> tapItem(2)
        }
    }
}