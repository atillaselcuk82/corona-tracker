package com.abnamro.apps.referenceandroid.ui_tests

import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.abnamro.apps.referenceandroid.TestHelper.assertElementDoesNotMatch
import com.abnamro.apps.referenceandroid.TestHelper.assertElementExists
import com.abnamro.apps.referenceandroid.TestHelper.assertElementExistsAtPosition
import com.abnamro.apps.referenceandroid.TestHelper.openActionBarMenu
import com.abnamro.apps.referenceandroid.TestHelper.scrollToPosition
import com.abnamro.apps.referenceandroid.TestHelper.swipeDown
import com.abnamro.apps.referenceandroid.TestHelper.swipeUp
import com.abnamro.apps.referenceandroid.TestHelper.tap
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.activeCases
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.countryCases
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.countryDeaths
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.countryList
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.countryName
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.countryRecovered
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.critical
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.deathRatio
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.filterButtonCoronaPerCountry
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.homeButton
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.swipeToRefreshCoronaPerCountryScreen
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.titleCoronaPerCountry
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.todayCases
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.todayDeaths
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.allCoronaCasesNumber
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.checkElementsCardviewAllCases
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.checkElementsCardviewDeaths
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.checkElementsCardviewRecovered
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.deathsNumber
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.filterButtonGlobalCoronaInfo
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.mockOff
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.mockOn
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.recoveredNumber
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.swipeToRefreshGlobalCoronaVirusInfo
import com.abnamro.apps.referenceandroid.page_objects.GlobalCoronavirusInformationScreen.titleGlobalCoronavirusInfo
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.deathRatioSeekBar
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.displayDetailsSwitch
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.displayDetailsText
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.highlightDeathRatioText
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.searchButton
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.showCountriesSpinner
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.showCoutriesText
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.sortBySpinner
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.sortByText
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.titleSortAndFilterCountries
import com.abnamro.apps.referenceandroid.view.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ElementsCheckOnFlowTest {

    @get:Rule
    //Open 'Global Coronavirus information' screen
    var activityRule: ActivityTestRule<MainActivity>
            = ActivityTestRule(MainActivity::class.java)

    @Test
    fun checkAllElementsInFlow() {
        /**
         * Given I am on the 'Global Coronavirus information' screen
         * When I tap the 'Mock OFF' menu item from the elipsis
         * Then the data from the endpoint (live data) is displayed
         */
        openActionBarMenu()
        tap(mockOff)
        assertElementDoesNotMatch(allCoronaCasesNumber) // 100000 -> mock data
        assertElementDoesNotMatch(deathsNumber) // 80000 -> mock data
        assertElementDoesNotMatch(recoveredNumber) // 40000 -> mock data

        /**
         * Given I am on the 'Global Coronavirus information' screen
         * When I tap the 'Mock ON' menu item from the elipsis
         * Then the mocked data is displayed
         */
        openActionBarMenu()
        tap(mockOn)
        assertElementExists(allCoronaCasesNumber)
        assertElementExists(deathsNumber)
        assertElementExists(recoveredNumber)

        /**
         * Given I am on the 'Global Coronavirus information' screen
         * Then I see the title
         * And I see 3 cardviews with coronavirus elements; cases, deaths and recovered
         */
        assertElementExists(titleGlobalCoronavirusInfo)
        checkElementsCardviewAllCases()
        checkElementsCardviewDeaths()
        checkElementsCardviewRecovered()

        /**
         * Given I am on the main screen
         * When I swipe down
         * Then the screen is refreshed
         */
        swipeDown(swipeToRefreshGlobalCoronaVirusInfo)
        assertElementExists(allCoronaCasesNumber)
        assertElementExists(deathsNumber)
        assertElementExists(recoveredNumber)

        /**
         * Given I am on the main screen
         * When I tap the filter button
         * Then the Sort & Filter screen is displayed
         */
        tap(filterButtonGlobalCoronaInfo)
        assertElementExists(titleSortAndFilterCountries)

        /**
         * Given I am on the 'Sort & Filter Countries' screen
         * Then I see the title
         * And I see the Sort by spinner elements
         * And I see the Show countries spinner elements
         * And I see the Display details switch elements
         * And I see the Highlight death-ratio seekbar elements
         * And I see a search button
         */
        assertElementExists(sortByText)
        assertElementExists(sortBySpinner)
        assertElementExists(showCoutriesText)
        assertElementExists(showCountriesSpinner)
        assertElementExists(displayDetailsText)
        assertElementExists(displayDetailsSwitch)
        assertElementExists(highlightDeathRatioText)
        assertElementExists(deathRatioSeekBar)
        assertElementExists(searchButton)

        /**
         * Given I am on the 'Sort & Filter Countries' screen
         * When I tap the search button
         * Then the 'Coronavirus per Country' screen is displayed
         */
        tap(searchButton)
        assertElementExists(titleCoronaPerCountry)

        /**
         * Given I am on the 'Coronavirus per Country' screen
         * When I scroll down to the bottom
         * Then I see Belgium_mock as last item of the list
         * And I see the data of its Corona cases
         */
        swipeUp(countryList)
        swipeUp(countryList)
        assertElementExists(countryName, "Belgium_mock")
        assertElementExists(countryCases,"Cases: 7284")
        assertElementExists(todayCases,"Today: 2")
        assertElementExists(activeCases, "Active: 6137")
        assertElementExists(countryDeaths, "Deaths: 289")
        assertElementExists(todayDeaths,"Today: 7")
        assertElementExists(deathRatio, "Death-ratio: 0,34")
        assertElementExists(countryRecovered, "Recovered: 858")
        assertElementExists(critical, "Critical: 690")

        /**
         * Given I am on the 'Coronavirus per Country' screen
         * When I am at the top of the list
         * And I swipe down
         * Then the screen is refreshed
         */
        scrollToPosition(countryList, 1)
        swipeDown(swipeToRefreshCoronaPerCountryScreen)
        assertElementExistsAtPosition(countryList, 0, countryName)

        /**
         * Given I am on the 'Coronavirus per Country' screen
         * And I see the filter button
         * When I tap the home button
         * Then the main screen is displayed
         */
        assertElementExists(filterButtonCoronaPerCountry)
        tap(homeButton)
    }
}
