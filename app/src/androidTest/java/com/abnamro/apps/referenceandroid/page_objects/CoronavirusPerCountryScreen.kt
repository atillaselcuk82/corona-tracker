package com.abnamro.apps.referenceandroid.page_objects

import com.abnamro.apps.referenceandroid.R

object CoronavirusPerCountryScreen {
    const val titleCoronaPerCountry = "Coronavirus per Country"
    const val swipeToRefreshCoronaPerCountryScreen = R.id.swiperefresh_country_list
    const val red = R.color.red

    //RecyclerView id
    const val countryList = R.id.country_list_rec_view

    //Recyclerview item elements
    const val countryName = R.id.county_name_tv
    const val countryCases = R.id.cases_tv
    const val todayCases = R.id.today_cases_tv
    const val activeCases = R.id.active_cases_tv
    const val countryDeaths = R.id.deaths_tv
    const val todayDeaths = R.id.today_deaths_tv
    const val deathRatio = R.id.death_ratio_tv
    const val countryRecovered = R.id.recovered_tv
    const val critical = R.id.critical_tv
    const val casesPerMillion = R.id.cases_per_million_tv
    const val deathsPerMillion = R.id.deaths_per_million_tv

    //Buttons
    const val homeButton = R.id.fab_home
    const val filterButtonCoronaPerCountry = R.id.fab_filter_corona_per_country
}