package com.abnamro.apps.referenceandroid

//data class to combine id and text of the views in page objects
data class TextId(val id: Int, val text: String)

object DataHelper {
    //hardcoded data-arrays of 'cases', 'deaths' and 'recovered' cases of the mocked-data.
    // Used to perform checks on the results after filtering and sorting
    val casesArray = arrayListOf(81394, 104256, 86498, 65719, 50871, 32964, 32332, 14543, 12928, 9478, 8603, 7697, 7284)
    val deathsArray = arrayListOf(3295, 1704, 9134, 5138, 351, 1995, 2378, 759, 231, 144, 546, 58, 289)
    val recoveredArray = arrayListOf(74971, 170, 90, 9357, 6658, 5700, 113, 135, 1530, 4811, 3, 225, 858)
}
