package com.abnamro.apps.referenceandroid

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.*
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Checkable
import android.widget.SeekBar
import com.abnamro.apps.referenceandroid.DataHelper.casesArray
import com.abnamro.apps.referenceandroid.DataHelper.deathsArray
import com.abnamro.apps.referenceandroid.DataHelper.recoveredArray
import com.abnamro.apps.referenceandroid.TestHelper.RecyclerViewMatchers.hasItemCount
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*

object TestHelper {

    /**
     * BUTTON INTERACTIONS
     */
    fun tap(id: Int) { onView(allOf(withId(id), isDisplayed())).perform(click()) }

    fun tap(text: String) { onView(allOf(withText(text), isDisplayed())).perform(click()) }

    fun tapItem(atPosition: Int) {
        val element = onData(
            allOf(`is`(instanceOf(String::class.java)))).atPosition(atPosition)
        element.perform(click())
    }

    fun openActionBarMenu() {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());

    }
    /**
     * CUSTOM ASSERTIONS
     */
    fun assertElementExists(id: Int) { onView(withId(id)).check(matches(isDisplayed())) }

    fun assertElementExists(text: String) { onView(withText(text)).check(matches(withText(text))) }

    fun assertElementExists(textId: TextId) {
        onView(allOf(withId(textId.id), isDisplayed()))
            .check(matches(withText(textId.text)))
    }

    fun assertElementExists(id: Int, text: String) {
        onView(allOf(withId(id), withText(text))).check(matches(isDisplayed()))
    }

    fun assertElementDoesNotMatch(textId: TextId) {
        onView(allOf(withId(textId.id), isDisplayed()))
            .check(matches(isDisplayed()))
    }

    /**
     * SWIPE & SCROLL FUNCTIONS
     */
    fun swipeDown(id: Int) { onView(withId(id)).perform(swipeDown()) }

    fun swipeUp(id: Int) { onView(withId(id)).perform(swipeUp()) }

    fun scrollToPosition(id: Int, position: Int) {
        onView(withId(id))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(position, scrollTo()))
    }
    /**
     * CHECKS THE STATE OF SWITCH AND THEN SWITHES IT (IF NEEDED)
     */
    fun switch(id: Int, isOn: Boolean) { onView(withId(id)).perform(setChecked(isOn)) }

    private fun setChecked(checked: Boolean): ViewAction? {
        return object : ViewAction {
            override fun getConstraints(): BaseMatcher<View?> {
                return object : BaseMatcher<View?>() {
                    override fun matches(item: Any): Boolean {
                        return isA(Checkable::class.java).matches(item)
                    }

                    override fun describeMismatch(
                        item: Any?,
                        mismatchDescription: Description?
                    ) {
                    }

                    override fun describeTo(description: Description?) {}
                }
            }

            override fun getDescription(): String? {
                return null
            }

            override fun perform(uiController: UiController, view: View) {
                val checkableView = view as Checkable
                checkableView.isChecked = checked
            }
        }
    }

    /**
     * MOVES THE SEEK BAR BY PROGRESS VALUE
     */
    fun seekBarValue(id: Int, value: Int) { onView(withId(id)).perform(setProgress(value)) }

    private fun setProgress(progress: Int): ViewAction? {
        return object : ViewAction {
            override fun perform(uiController: UiController, view: View) {
                val seekBar = view as SeekBar
                seekBar.progress = progress
            }

            override fun getDescription(): String {
                return "Set a progress on a SeekBar"
            }

            override fun getConstraints(): Matcher<View> {
                return isAssignableFrom(SeekBar::class.java)
            }
        }
    }

    /**
     * RECYCLERVIEW ASSERTIONS
     */
    fun assertListSizeEqualsTo(id: Int, expectedCount: Int) {
        onView(withId(id)).check(matches((hasItemCount(expectedCount))))
    }

    object RecyclerViewMatchers {
        fun hasItemCount(itemCount: Int): Matcher<View> {
            return object : BoundedMatcher<View, RecyclerView>(
                RecyclerView::class.java) {

                override fun describeTo(description: Description) {
                    description.appendText("has $itemCount items")
                }

                override fun matchesSafely(view: RecyclerView): Boolean {
                    return view.adapter?.itemCount == itemCount
                }
            }
        }
    }

    fun assertElementExistsAtPosition(listId: Int, position: Int, text: String){
        onView(withId(listId))
            .check(matches(atPosition(position, hasDescendant(withText(text)))));
    }

    fun assertElementExistsAtPosition(listId: Int, position: Int, id: Int){
        onView(withId(listId))
            .check(matches(atPosition(position, hasDescendant(withId(id)))));
    }

    fun assertElementExistsAtPositionWithColor(listId: Int, position: Int, color: Int){
        onView(withId(listId))
            .check(matches(atPosition(position, hasDescendant(hasTextColor(color)))))
    }

    fun assertElementNOTExistsAtPosition(listId: Int, position: Int, id: Int){
        onView(withId(listId))
            .check(matches(atPosition(position, not(hasDescendant(withId(id))))));
    }

    private fun atPosition(position: Int, itemMatcher: Matcher<View?>): Matcher<View?>? {
        return object: BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder =
                    view.findViewHolderForAdapterPosition(position)
                        ?: // has no item on such position
                        return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }

    fun assertListIsSortedBy(listId: Int, text: String, listSize: Int) {
        val list: List<Int> = when (text) {
            "Cases" -> casesArray.sortedDescending()
            "Deaths" -> deathsArray.sortedDescending()
            "Recovered" -> recoveredArray.sortedDescending()
            else -> casesArray.sortedDescending()
        }
        for (position in 0 until listSize) {
            scrollToPosition(listId, position)
            val textToCompare = "$text: ${list[position]}"
            assertElementExistsAtPosition(listId, position, textToCompare)
        }
    }
}
