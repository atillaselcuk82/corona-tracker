package com.abnamro.apps.referenceandroid.page_objects

import com.abnamro.apps.referenceandroid.R
import com.abnamro.apps.referenceandroid.TestHelper.assertElementExists
import com.abnamro.apps.referenceandroid.TextId

object GlobalCoronavirusInformationScreen {
    const val titleGlobalCoronavirusInfo = "Global Coronavirus information"
    const val swipeToRefreshGlobalCoronaVirusInfo = R.id.fragment
    const val filterButtonGlobalCoronaInfo = R.id.fab_filter_mainscreen

    // Vertical elipsis menu items to (de)mock data
    const val mockOn = "mock ON"
    const val mockOff = "mock OFF"

    // elements from cardview "Coronavirus cases'
    private val globalTextCardview1 = TextId(R.id.global1, "Global")
    private val allCoronaCasesText = TextId(R.id.all_corona_cases_tv, "Coronavirus cases")
    val allCoronaCasesNumber = TextId(R.id.number_of_all_corona_cases_tv, "100000")

    // elements from cardview "Deaths'
    private val globalTextCardview2 = TextId(R.id.global2, "Global")
    private val deathsText = TextId(R.id.deaths_tv, "Deaths")
    val deathsNumber = TextId(R.id.number_of_deaths_tv, "80000")

    // elements from cardview "Recovered"
    private val globalTextCardview3 = TextId(R.id.global3, "Global")
    private val recoveredText = TextId(R.id.recovered_tv, "Recovered")
    val recoveredNumber = TextId(R.id.number_of_recovered_tv, "40000")

    fun checkElementsCardviewAllCases() {
        assertElementExists(globalTextCardview1)
        assertElementExists(allCoronaCasesText)
        assertElementExists(allCoronaCasesNumber)
    }

    fun checkElementsCardviewDeaths() {
        assertElementExists(globalTextCardview2)
        assertElementExists(deathsText)
        assertElementExists(deathsNumber)
    }

    fun checkElementsCardviewRecovered() {
        assertElementExists(globalTextCardview3)
        assertElementExists(recoveredText)
        assertElementExists(recoveredNumber)
    }
}