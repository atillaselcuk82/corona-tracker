package com.abnamro.apps.referenceandroid.ui_tests.classification_tree_method_tests

import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.abnamro.apps.referenceandroid.TestHelper.assertElementExists
import com.abnamro.apps.referenceandroid.TestHelper.assertElementExistsAtPosition
import com.abnamro.apps.referenceandroid.TestHelper.assertListIsSortedBy
import com.abnamro.apps.referenceandroid.TestHelper.assertListSizeEqualsTo
import com.abnamro.apps.referenceandroid.TestHelper.seekBarValue
import com.abnamro.apps.referenceandroid.TestHelper.switch
import com.abnamro.apps.referenceandroid.TestHelper.tap
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.casesPerMillion
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.countryList
import com.abnamro.apps.referenceandroid.page_objects.CoronavirusPerCountryScreen.deathsPerMillion
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.deathRatioSeekBar
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.displayDetailsSwitch
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.searchButton
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.selectItemShowCountries
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.selectItemSorBySpinner
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.showCountriesSpinner
import com.abnamro.apps.referenceandroid.page_objects.SortAndFilterCountriesScreen.sortBySpinner
import com.abnamro.apps.referenceandroid.view.FilterAndSortActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class TestCase3 {

    /**
     * TEST CASE 3:
     * ------------------------------------------------------
     * Given I am on the 'Sort & Filter Countries screen
     * When I select 'recovered' from 'Sort by' spinner
     * And I select 'Top 10' from 'Show countries' spinner
     * And the 'Display details' switch is ON
     * And the Seekbar 'highlight death-ratio' is 100
     * And I tap the search FAB
     * Then the 'Coronavirus per Country' screen is displayed
     * And I see a sorted list by cases of all countries
     * And I see detailed information
     * And highlights of death-cases above 100 are displayed
     * -----------------------------------------------------
     */

    @get:Rule
    //Open Sort & Filter screen
    var activityRule: ActivityTestRule<FilterAndSortActivity>
            = ActivityTestRule(FilterAndSortActivity::class.java)

    @Test
    fun filterAndSortTest() {
        // Select 'recovered' from 'Sort by' spinner
        tap(sortBySpinner)
        selectItemSorBySpinner("recovered")

        // Select 'top 10' from 'Show countries' spinner
        tap(showCountriesSpinner)
        selectItemShowCountries("top 10")

        // Turn display details switch on
        switch(displayDetailsSwitch, true)

        // seekbar value set to 100
        seekBarValue(deathRatioSeekBar, 100)

        // tap search button and check if country list screen is opened
        tap(searchButton)
        assertElementExists("Coronavirus per Country")

        assertListIsSortedBy(countryList, "Recovered", 10)
        assertListSizeEqualsTo(countryList, 10)
        assertElementExistsAtPosition(countryList, 9, casesPerMillion)
        assertElementExistsAtPosition(countryList, 9, deathsPerMillion)
    }
}
