package com.abnamro.apps.referenceandroid.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Spinner
import android.widget.Toast
import com.abnamro.apps.referenceandroid.R
import kotlinx.android.synthetic.main.activity_filter_and_sort.*

class FilterAndSortActivity : AppCompatActivity() {

    var seekBarValue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_and_sort)
        setSpinner(sort_by_spinner, R.array.sort_by)
        setSpinner(show_countries_spinner, R.array.show_countries)
        initializeWidgets()
        setSeekbar()
        fab_search.setOnClickListener { sendFilterAndSortValues() }
    }

    private fun initializeWidgets() {
        seek_bar_value_tv.text = 0.toString()
        display_details_switch.isChecked = false
    }

    private fun setSeekbar() {
        death_ratio_seek_bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                seekBarValue = i
                seek_bar_value_tv.text = i.toString()
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    private fun setSpinner(spinner: Spinner, arrayId: Int) {
        val list = resources.getStringArray(arrayId)
        val layout = R.layout.spinner_item
        val adapter = ArrayAdapter(this, layout, list)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    private fun sendFilterAndSortValues() {
        val intent = Intent(this,CoronaByCountryListActivity::class.java)
        val sortBy = sort_by_spinner.selectedItem.toString()
        val showCountries = show_countries_spinner.selectedItem.toString()
        val isChecked = display_details_switch.isChecked

        intent.putExtra("sortBy",sortBy)
        intent.putExtra("showCountries", showCountries)
        intent.putExtra("isChecked", isChecked)
        intent.putExtra("highlightFrom", seekBarValue)

        startActivity(intent)
    }
}
