package com.abnamro.apps.referenceandroid.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.abnamro.apps.referenceandroid.R
import com.abnamro.apps.referenceandroid.model.CoronaPerCountry
import com.abnamro.apps.referenceandroid.model.CoronaPerCountryApiService
import com.abnamro.apps.referenceandroid.model.SharedPrefs.getMockStatus
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_corona_by_country_list.*

class CoronaByCountryListActivity : AppCompatActivity() {

    private val coronaPerCountryApiServe by lazy { CoronaPerCountryApiService.create() }
    private var disposable: Disposable? = null
    private lateinit var adapter : CountryListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_corona_by_country_list)
        fetchData()
        fab_home.setOnClickListener { goToMainActivity() }
        fab_filter_corona_per_country.setOnClickListener { goToFilterAndSortActivity() }
        swiperefresh_country_list.setOnRefreshListener {
            fetchData()
            swiperefresh_country_list.isRefreshing = false
        }
    }

    private fun goToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun goToFilterAndSortActivity() {
        val intent = Intent(this, FilterAndSortActivity::class.java)
        startActivity(intent)
    }

    private fun fetchData() {
        if (getMockStatus("mock", this)) {
            fetchMockedData()
        } else {
            disposable = coronaPerCountryApiServe.getCountryDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result -> populateRecyclerView(filterAndSort(result)) },
                    { error -> Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show() }
                )
        }
    }

    private fun fetchMockedData() {
        val mapper = jacksonObjectMapper()
        val fileInString: String =
            applicationContext.assets.open("countries.json").bufferedReader().use { it.readText() }
        val countries = mapper.readValue<ArrayList<CoronaPerCountry>>(fileInString)
        populateRecyclerView((filterAndSort(countries)))
    }


    private fun filterAndSort(countryList: ArrayList<CoronaPerCountry>): ArrayList<CoronaPerCountry> {
        var list = countryList.toList()
        val sortBy=intent.getStringExtra("sortBy")
        val showCountries=intent.getStringExtra("showCountries")

        list = when(sortBy) {
            "deaths" -> list.sortedByDescending { it.deaths }
            "recovered" -> list.sortedByDescending { it.recovered }
            else -> list.sortedByDescending { it.cases }
        }

        list = when(showCountries) {
            "top 10" -> list.filterIndexed { index, _ -> (index < 10) }
            "top 3" -> list.filterIndexed { index, _ -> (index < 3) }
            else -> list.filterIndexed { index, _ -> (index >= 0) }
        }
        return ArrayList(list)
    }

    private fun populateRecyclerView(countryList: ArrayList<CoronaPerCountry>) {
        val showDetail = intent.getBooleanExtra("isChecked", false)
        val highLightFrom = intent.getIntExtra("highlightFrom", 0)
        country_list_rec_view.layoutManager = LinearLayoutManager(this)
        adapter = CountryListAdapter(countryList, showDetail, highLightFrom)
        country_list_rec_view.adapter = adapter
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}
