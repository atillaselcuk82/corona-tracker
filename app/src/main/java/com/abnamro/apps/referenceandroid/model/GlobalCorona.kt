package com.abnamro.apps.referenceandroid.model

class GlobalCorona(
    val cases: Int,
    val deaths: Int,
    val recovered: Int
)