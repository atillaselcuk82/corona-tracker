package com.abnamro.apps.referenceandroid.viewmodel

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.abnamro.apps.referenceandroid.model.GlobalCoronaApiService
import com.abnamro.apps.referenceandroid.R
import com.abnamro.apps.referenceandroid.model.GlobalCorona
import com.abnamro.apps.referenceandroid.model.SharedPrefs.getMockStatus
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main.*

class MainActivityFragment : Fragment() {

    private val globalCoronaApiServe by lazy { GlobalCoronaApiService.create() }
    private var disposable: Disposable? = null
    private var isMock: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isMock = getMockStatus("mock", view.context)
        fetchData()
        swiperefresh.setOnRefreshListener {
            fetchData()
            swiperefresh.isRefreshing = false
        }
    }

    private fun fetchData() {
        if (isMock) {
            fetchMockedData()
        } else {
            fetchDataFromEndpoint()
        }
    }

    private fun fetchDataFromEndpoint() {
        disposable = globalCoronaApiServe.getAllCoronaCases()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    number_of_all_corona_cases_tv.text = "${result.cases}"
                    number_of_deaths_tv.text = "${result.deaths}"
                    number_of_recovered_tv.text = "${result.recovered}"
                },
                { error -> Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show() }
            )
    }

    private fun fetchMockedData() {
        val mapper = jacksonObjectMapper()
        val fileInString: String =
            activity!!.applicationContext.assets.open("all.json").bufferedReader().use { it.readText() }
        val allCases = mapper.readValue<GlobalCorona>(fileInString)
        number_of_all_corona_cases_tv.text = "${allCases.cases}"
        number_of_deaths_tv.text = "${allCases.deaths}"
        number_of_recovered_tv.text = "${allCases.recovered}"
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}
