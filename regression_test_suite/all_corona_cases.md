# All Corona Cases

| Testcases | OK/NOK |
| ------ | ------ |
|**Scenario: Title Main Screen**<br>Given I open the app<br>Then main screen opens<br>And the title of main screen is shown | |
|**Scenario: Elements check of CardViews**<br>Given I am on the main screen<br>Then 3 cardviews are shown with title 'Global'<br>And subtitles with respectively 'Coronavirus cases', 'Deaths' and 'Recovered'<br>And the number of each case under the subtitle|  |
|**Scenario: FAB check**<br>Given I am on the main screen<br>Then a FAB is shown on bottom right with a search icon||
|**Scenario: Elipsis check**<br>Given I am on the main screen<br>When I tap the vertical elipsis on top right<br>Then the menu item 'Settings' is displayed ||
|**Scenario: Swipe to refresh**<br>Given I am on the main screen<br>When I swipe the screen down<br>Then I the refesh icon showing up<br>And the data is refreshed ||
