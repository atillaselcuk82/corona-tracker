                               # Coronavirus per Country

| Testcases | OK/NOK |
| ------ | ------ |
|**Scenario: Title Screen**<br> Given I open the app<br>When I tap the FAB<br>Then the Coronavirus per country screen opens with title displayed on top|  |
|**Scenario: Data per country**<br> Given I am on the Coronavirus per country screen<br>Then a list of cardviews are shown with country name as title <br>And all 9 data info is shown as described in the [description](https://gitlab.com/atillaselcuk82/corona-tracker/-/issues/2)|  |
|**Scenario: Sorted by total case**<br> Given I am on the Coronavirus per country screen<br>Then the list of countries are sorted by total number of cases||
|**Scenario: Home button**<br> Given I am on the Coronavirus per country screen<br>When I tab the home-FAB<br>Then the main screen is displayed||
|**Scenario: Swipe to refresh**<br> Given I am on the Coronavirus per country screen<br>When I swipe down to refresh<br>Then the data is refreshed ||
