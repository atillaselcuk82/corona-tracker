# waits for an Android Virtual Device what has booted completely and ready for running tests

set +e

bootanim=""
failcounter=0
timeout_in_sec=300

until [[ "$bootanim" =~ "stopped" ]]; do
  bootanim=$("$ANDROID_HOME"/platform-tools/adb -e shell getprop init.svc.bootanim 2>&1 &)
  echo "$bootanim"
  if [[ "$bootanim" =~ "not found"
    || "$bootanim" =~ "device not found"
    || "$bootanim" =~ "no emulators found"
    || "$bootanim" =~ "device offline"
    || "$bootanim" =~ "running" ]]; then
    let "failcounter += 5"
    echo "Waiting for emulator to start"
    if [[ $failcounter -gt timeout_in_sec ]]; then
      echo "Timeout ($timeout_in_sec seconds) reached; failed to start emulator"
      jobs -l
      exit 1
    fi
  fi
  sleep 5
done

echo "Emulator is ready!"
exit 0