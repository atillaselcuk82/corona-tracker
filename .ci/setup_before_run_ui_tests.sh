# starts an Android Virtual Device with a snapshot
"$ANDROID_HOME"/emulator/emulator -avd Nexus_5X_API_28 -no-audio -no-window &

# waits for the Android Virtual Device to boot completely
./.ci/wait_for_emulator.sh

# disable animations
"$ANDROID_HOME"/platform-tools/adb shell settings put global window_animation_scale 0
"$ANDROID_HOME"/platform-tools/adb shell settings put global transition_animation_scale 0
"$ANDROID_HOME"/platform-tools/adb shell settings put global animator_duration_scale 0
