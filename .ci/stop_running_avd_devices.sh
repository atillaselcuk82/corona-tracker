# stops all the running Android Virtual Devices
"$ANDROID_HOME"/platform-tools/adb devices | grep emulator | cut -f1 | while read line; do "$ANDROID_HOME"/platform-tools/adb -s "$line" emu kill; done

exit "$exit_code"