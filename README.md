# Summary
This project is an Android application displaying the global Corona cases as well as cases per country with some filter options. The purpose is to write structured UI tests using the Classification Tree Model CTM (Data Combination Test in dutch); which is a Tmap - Test Design Technique.

# Screens
Tha app consists of simply 3 screens. From left to right these are:
1. **The main screen:** The app opens in this screen. In this screen the world wide totals of corona cases, deaths and recovered ones are displayed <br>
2. **The Sort & Filter screen:** In this screen there are 2 sort and 2 filter options. When these options are set and the search button is clicked, then a list of Coronavirus cases per country are listed, sorted and filtered by the options set. The sort and filter options are the following:
   * __sort by__: which will sort the data by: 'total cases', 'deaths' and 'recoverd' ones by country.
   * __show countries__: which wil display the 'top 3', 'top 10' or 'all' countries
   * __display details__: when the toggle is turned on, additional data is displayed per country
   * __highlight death ratio__: a seek bar from 0 to 100 which, when set, will highlight the death cases above this number
3. **The Coronavirus per sountry screen:** which will display the sorted and filtered list. A 'home' button and a 'filter' button are displayed to respectively go back to the home screen and sort & filter screen.

<img src="screen_images/homescreen.jpg" alt="drawing" width="220" /> &nbsp;&nbsp;&nbsp; <img src="screen_images/sort_and_filter_screen.jpg" alt="drawing" width="220"/> &nbsp;&nbsp;&nbsp; <img src="screen_images/country_list_screen.jpg" alt="drawing" width="220"/>

# Project Setup
The 'corona-tracker' app is created in a structured way. It is divided in small parts which are described as stories. Theses stories are created and sorted in order of development as tickets on the [Gitlab Board](https://gitlab.com/atillaselcuk82/corona-tracker/-/boards/1624570). The Story Board has 5 lains which are the states in which a story can be. These state are the following:
1. **Open**: Unrefined stories describing the work that needs to be done.
2. **To Do**: Stories that are refined and are ready to be picked up (DOR are met).
3. **Doing**: Stories that are started with development
4. **QA**: Stories that are finished with development and ready for testing
5. **Closes**: DOR criteria are met

# Definition of Ready (DOR)
All stories need to be refined before they can be picked up for development. In this project we have a set of rules that need to be satisfied before they can be picked up. these rules are defined in the 'Definition of Ready':

| Definition of Ready                             | OK/NOK |
|-------------------------------------------------|--------|
| User Story is in format <As a user I want .. >| |        |
| Description is available                        |        |
| All Acceptance Criteria have been defined       |        |
| The Product Risk Analysis is ready              |        |
| The Story Points are defined                    |        |

# Definition of Done (DOD)
All stories must satisfy the set of rules which are described in the 'Definition of Done' before they can be closed. The set of DOD rules for this project are as follows:

| Definition of Done                              | OK/NOK |
|-------------------------------------------------|--------|
| All code has been completed                     |        |
| All tests succeed                               |        |
| All Acceptance Criteria are tested and approved |        |
| Relevant documentation is available             |        |

# Testing
Testing of the project is done in the following steps:
1. **Test cases from Acceptance Criteria**: The test cases are first derived from the acc. criteria and are written with Gherkin syntax. The syntax is easy to write and understand. These test cases are also displayd in table form in the comment of each story stating with OK or NOK wether the test is passed or not.
2. **Regression Test Suite**: The test cases that are created from the acceptance criteria are stored in the [Regression Test Suite](https://gitlab.com/atillaselcuk82/corona-tracker/-/tree/master/regression_test_suite). This folder consist of markdown files for every feature. The scenarios for every feature are described within each file in Gherkin syntax (Given, When, Then). The Regression Test Suite is a growing document; for every new feature there will be acceptance criteria which eventually will be developed into test cases. These test cases will be added a as new feature file or updated in the proper existing feature file.
3. **Classification Tree Model**: The Classification Tree Model is used to create a minimum set of test cases to obtain maximum coverage of the Sort & Filter screen. This document can be found in the [documentation](https://gitlab.com/atillaselcuk82/corona-tracker/-/tree/master/documentation) folder.
4. **UI Automation test**: The UI test automation is written with the Espresso library. Page Object Model is used to enhance test maintenance and reducing code duplication. The UI are automated by

# CI/CD
The ci/cd pipline is set-up within the gitlab-ci.yml file. The [template](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Android.gitlab-ci.yml) from gitlab is used. For further readings you can read the Gitlab documentation [here](https://about.gitlab.com/blog/2018/10/24/setting-up-gitlab-ci-for-android-projects/). In short the pipeline consists of 2 stages each with 1 or more jobs. These are:
1. **stage**: _build_
    * _job_: assembleDebug
    * _job_: lintDebug
2. **stage**: _test_
    * _job_: debugTest

The build stage ensures the app compiles, and the test stage runs our unit and functional tests.


